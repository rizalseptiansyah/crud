<?php

require_once 'connection.php';

?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>crud</title>
</head>
<body>

    <div>
        <form action="add.php" method="post">
            <input type="text" name="product_name" placeholder="Product's Name">
            <input type="number" name="price" placeholder="Price">
            <input type="number" name="qty" placeholder="Qty">
            <input type="submit" name="submit" value="Add Data">
        </form>
    </div>

    <table border="1">
        <?php
        $q = $conn->query("SELECT * FROM tb_product");

        $no = 1;
        if($q->num_rows > 0) {
        ?>
        <tr>
            <th>No.</th>
            <th>Product Name</th>
            <th>Price</th>
            <th>Qty</th>
            <th>Aksi</th>
        </tr>
        <?php    
            while($dt = $q->fetch_assoc()) {
        ?>
        <tr>
            <td><?= $no++ ?></td>
            <td><?= $dt['product_name'] ?></td>
            <td><?= $dt['price'] ?></td>
            <td><?= $dt['qty'] ?></td>
            <td>
                <a href="update.php?id=<?= $dt['product_id'] ?>">edit</a>
                <a href="delete.php?id=<?= $dt['product_id'] ?>" onclick="return confirm('Anda yakin akan menghapus data ini?')">delete</a>
            </td>
        </tr>
        <?php
            }
        } else {
        ?>
        <h2>data tidak ditemukan</h2>
        <a href="#">add data</a>
        <?php
        }
        ?>
    </table>

    <?php

    ?>
    
</body>
</html>